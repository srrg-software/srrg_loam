#include "scan_registration.h"
#include <utils/scan_utils.h>

namespace srrg_loam {

  ScanRegistration::ScanRegistration() {
    _is_init = false;
    _multi_scan_mapper = 0;
    _laser_cloud_scans = new CloudXYZIVector();
  }

  ScanRegistration::~ScanRegistration() {
    delete _laser_cloud_scans;
    std::cerr << "[ScanRegistration]: destroyed." << std::endl;
  }

  void ScanRegistration::setMultiScanMapper(MultiScanMapper* multi_scan_mapper_) {
    _is_init = false;
    _multi_scan_mapper = multi_scan_mapper_;
  }

  void ScanRegistration::setScan(const Time& scan_time_, CloudXYZ* laser_cloud_) {
    _scan_time = scan_time_;
    _laser_cloud = laser_cloud_;
  }

  void ScanRegistration::setImuData(const Time& imu_time_, const ImuState& imu_state_) {
    _imu_time = imu_time_;
    _imu_state = imu_state_;
  }

  void ScanRegistration::setup() {
    if(!_multi_scan_mapper)
      throw std::runtime_error("[ScanRegistration][setup]: missing MultiScanMapper!");
    _is_init = true;    
  }

  void ScanRegistration::projectPointToStartOfSweep(pcl::PointXYZI& point_, const real& relative_time_) {
    // project point to the start of the sweep using corresponding IMU data

    // TODO: embed IMU
  }

  void ScanRegistration::processScanLines() {
    _detector.setLaserScans(_laser_cloud_scans);
    _detector.compute();
  }

  void ScanRegistration::compute() {
    if(!_is_init)
      throw std::runtime_error("[ScanRegistration][compute]: call setup first!");
    if(!_laser_cloud)
      throw std::runtime_error("[ScanRegistration][compute]: call setScan first!");
    
    size_t cloud_size = _laser_cloud->size();
    //bdc determine start and end orientation of the scan
    real start_orientation, end_orientation;

    const pcl::PointXYZ& first_point = _laser_cloud->points[0];
    const pcl::PointXYZ& last_point = _laser_cloud->points[cloud_size - 1];

    getStartEndOrientation(start_orientation, end_orientation,
                           first_point, last_point);

    bool half_passed = false;
    pcl::PointXYZI point;
    _laser_cloud_scans->resize(_multi_scan_mapper->numScanRings());
    // clear all laser_cloud points
    std::for_each(_laser_cloud_scans->begin(), _laser_cloud_scans->end(), [](auto&&v) {v.clear();});

    //bdc clean cloud and add intensity value based on the distance
    for(size_t i = 0; i < cloud_size; ++i) {
      const pcl::PointXYZ& cloud_point = _laser_cloud->points[i];
      if(!pcl::isFinite(cloud_point))
        continue;
      if(squaredNorm(cloud_point) < 1e-4)
        continue;

      // calculate vertical point angle and scan ID
      real angle = std::atan(cloud_point.z / std::sqrt(cloud_point.y * cloud_point.y + cloud_point.x * cloud_point.x));
      int scanID = _multi_scan_mapper->getRingForAngle(angle);
      if (scanID >= _multi_scan_mapper->numScanRings() || scanID < 0)
        continue;

      // calculate horizontal point angle
      real orientation = -std::atan2(cloud_point.y, cloud_point.x);
      if (!half_passed) {
        if (orientation < start_orientation - M_PI / 2)
          orientation += 2 * M_PI;
        else if (orientation > start_orientation + M_PI * 3 / 2)
          orientation -= 2 * M_PI;
        if (orientation - start_orientation > M_PI)
          half_passed = true;
      } else {
        orientation += 2 * M_PI;
        if (orientation < end_orientation - M_PI * 3 / 2)
          orientation += 2 * M_PI;
        else if (orientation > end_orientation + M_PI / 2)
          orientation -= 2 * M_PI;
      }

      // calculate relative scan time based on point orientation
      real relative_time = config().scan_period * (orientation - start_orientation) / (end_orientation - start_orientation);
      point.x = cloud_point.y;  // notice the frame rotation here
      point.y = cloud_point.z;
      point.z = cloud_point.x;
      point.intensity = scanID + relative_time;

      projectPointToStartOfSweep(point, relative_time );
      _laser_cloud_scans->at(scanID).points.push_back(point);
    }
    
    processScanLines();

  }
    
}
