#pragma once

#include "defs.h"

namespace srrg_loam {

  struct ImuState{
    // @brief c'tor
    ImuState();

    // @brief interpolate between two ImuState
    static void interpolate(const ImuState& start_,
                            const ImuState& end_,
                            const real& ratio_,
                            ImuState& result_);
    
    Time stamp;
    Vector3 orientation;  // current roll/pitch/yaw
    Vector3 position;     // global position
    Vector3 velocity;     // global velocity
    Vector3 acceleration; // local acceleration
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };
  
}
