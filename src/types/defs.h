#pragma once

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <chrono>

namespace srrg_loam {
  
  //bdc Eigen/math definitions
  
  using real = float;
  
  using Vector3 = Eigen::Matrix<real, 3, 1>;

  using Vector6 = Eigen::Matrix<real, 6, 1>;
 
  using Isometry3 = Eigen::Transform<real, 3, Eigen::Isometry>;

  //bdc other definitions
  
  using Time = std::chrono::system_clock::time_point;

  using IndexRange = std::pair<size_t, size_t>;

  enum PointLabel {
    SURFACE_FLAT = -1,      ///< flat surface point
    SURFACE_LESS_FLAT = 0,  ///< less flat surface point
    CORNER_LESS_SHARP = 1,  ///< less sharp corner point
    CORNER_SHARP = 2        ///< sharp corner point
  };



  using CloudXYZ = pcl::PointCloud<pcl::PointXYZ>;
  using CloudXYZVector = std::vector<CloudXYZ>;

  using CloudXYZI = pcl::PointCloud<pcl::PointXYZI>;
  using CloudXYZIVector = std::vector<CloudXYZI>;
  
}
